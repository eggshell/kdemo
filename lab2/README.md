# Lab 2: Creating a Serverless Application

So, you've deployed a serverless function that you can call on demand. But what
if you want to spin up an entire application instead of just a function?
This lab will show you how to do so.

## About the Application

The application we will use is a web frontend written with golang and webassembly.
The boilerplate has been provided for you in the `app` directory of this repo.
You do not need to edit these files in any way, and they have been created for
you in the interest of time.

In the next steps of this lab, we will explore:

* Creating a Dockerfile for your serverless application.
* Create a GitLab personal access token.
* Use that token in GitLab CI/CD.
* Build an image for the application in GitLab CI/CD while leveraging Kaniko, a
  tool used for building container images inside of Kubernetes pods.
* Create a more robust CI/CD pipeline which will deploy your serverless function
  you created in Lab 1 and use it to populate information in the serverless
  application.

## Add a Dockerfile

In order for our serverless application to run on Knative, we will need to
build a container image for it. This will take place in CI, but we will first
need to add a file which defines how this container image is built.

Create a file named `Dockerfile` in the root directory of your project with
the following contents:

```
FROM golang:1.12.4
COPY . .
RUN GOOS=js GOARCH=wasm go build -o app/lib.wasm app/main.go
ENTRYPOINT ["./entrypoint.sh"]
EXPOSE 8080
```

Set the commit message to something like `Add Dockerfile`, then click the green
**Commit changes** button.

## Create Access Token

In order for your CI/CD pipeline to proceed, you will need to create a GitLab
personal access token. Personal access tokens are the preferred way for third
party applications and scripts to authenticate with the GitLab API, and in the
context of this lab will be used as a way to authenticate with the GitLab
container registry.

To create a personal access token, click on your user in the top right corner
of the GitLab UI, click **Settings**, then click **Access Tokens** in the left
hand sidebar.

Name your token anything you want, do not set an expiration date, and ensure
that all **four** check boxes are checked. Click the green **Create Personal Access
Token** button. The page will refresh, and your new personal access token will
be displayed.

**MAKE SURE YOU COPY THIS PERSONAL ACCESS TOKEN TO YOUR CLIPBOARD BEFORE YOU CLOSE THE TAB**.
Otherwise, you will need to generate a new token, as it will not be displayed
for you again.

## Add Access Token to CI/CD variables

Now that you have a token, you will need to add it to your CI/CD configuration
as an environment variable. Hover over **Settings** at the bottom of the
left-hand sidebar, then click on **CI/CD**.

Click the button labeled **Expand** next to the **Variables** section. In the
box labeled **Input Variable Key**, type `CI_DEPLOY_PASSWORD`, and in the box
labeled **Input Variable Value**, paste your personal access token.

Click the toggle switch for **Masked** so that it is greyed out. The contents
of your personal access token will not be dumped to CI output logs, but rather
referenced as an environment variable. If you do not do this, you will not be
allowed to apply your changes.

When you are done, click the green **Save variables** button.

## Update .gitlab-ci.yml

We will need to update our CI/CD configuration to build and deploy our
serverless application. First, we'll need to edit the `stages` section to add a
new stage:

```yaml
stages:
  - deploy-function
  - build-app
  - deploy-app
```

Next, we'll need to edit our `deploy-function` stage created in the last
section to look like this:

```yaml
deploy-hello-function:
  stage: deploy-function
  environment: test
  image: gcr.io/triggermesh/tm:latest
  before_script:
    - echo $TMCONFIG > tmconfig
    - tm --config ./tmconfig set registry-auth gitlab-registry --registry "$CI_REGISTRY" --username "$CI_REGISTRY_USER" --password "$CI_JOB_TOKEN" --push
    - tm --config ./tmconfig set registry-auth gitlab-registry --registry "$CI_REGISTRY" --username "$CI_DEPLOY_USER" --password "$CI_DEPLOY_PASSWORD" --pull
  script:
    - tm --config ./tmconfig deploy --wait; echo
  after_script:
    - export URL=$(tm --config ./tmconfig get service | grep knative-demo-hello | awk '{print $4}')
    - echo $URL | tee url.txt
  artifacts:
    paths:
      - url.txt
    expire_in: 1 day
```

Now that we have added some stages, we will need to define what it does. Add the
following block after the `stages` block, but before the `deploy-hello-function`
block:

```yaml
sample-app-build:
  stage: build-app
  image:
    name: gcr.io/kaniko-project/executor:debug-v0.6.0
    entrypoint: [""]
  before_script:
    - export URL=$(wget -qO- https://gitlab.com/$CI_DEPLOY_USER/kdemo/-/jobs/artifacts/master/raw/url.txt\?job\=deploy-hello-function)
    - export TIME=$(wget -qO- $URL | grep -o '[0-2][0-9]:[0-5][0-9]:[0-5][0-9]')
    - sed -i -e "s/REPLACE_WITH_TIME/$TIME/g" $CI_PROJECT_DIR/app/index.html
  script:
    - /busybox/echo "{\"auths\":{\"$CI_REGISTRY\":{\"username\":\"$CI_REGISTRY_USER\",\"password\":\"$CI_JOB_TOKEN\"}}}" > /kaniko/.docker/config.json
    - /kaniko/executor --context $CI_PROJECT_DIR --dockerfile $CI_PROJECT_DIR/Dockerfile --destination $CI_REGISTRY_IMAGE/sample-app:latest
  only:
    - master
```

Finally, add this block immediately after the `sample-app-build` block:

```yaml
deploy-hello-application:
  stage: deploy-app
  environment: test
  image: gcr.io/triggermesh/tm:latest
  before_script:
    - echo $TMCONFIG > tmconfig
    - tm --config ./tmconfig set registry-auth gitlab-registry --registry "$CI_REGISTRY" --username "$CI_REGISTRY_USER" --password "$CI_JOB_TOKEN" --push
    - tm --config ./tmconfig set registry-auth gitlab-registry --registry "$CI_REGISTRY" --username "$CI_DEPLOY_USER" --password "$CI_DEPLOY_PASSWORD" --pull
  script:
    - tm --config ./tmconfig deploy service sample-app -f "$CI_REGISTRY_IMAGE/sample-app:latest" --env SIMPLE_MSG="Hello GitLab $CI_JOB_URL" --wait; echo
  only:
    - master
```

Set the commit message to something like `Update .gitlab-ci.yaml`, then click
the green **Commit changes** button.

## Check the Pipeline

Click the **CI/CD** tab in the left-hand sidebar, then click on the most
recent pipeline. You will notice that there are now two stages, and your build
stage should be in **Running** state. You can click on the individual jobs to
get their output logs.

When the pipeline completes, the output of the `deploy-hello-application` job
will look like:

```shell
$ tm --config ./tmconfig deploy service sample-app -f "$CI_REGISTRY_IMAGE/sample-app:latest" --env SIMPLE_MSG="Hello GitLab $CI_JOB_URL" --wait
Creating sample-app function
Waiting for sample-app ready state
Service sample-app URL: http://sample-app-workshop-user1.k.triggermesh.io
```

The last line contains the URL for your application.

## Test the Application

Copy the URL from your job output log and paste it into a browser window. You
will be greeted with a message containing the job number used to deploy your
application. If you re-run the pipeline, you will see that this number will
increment!

## Continue to Lab 3

Please continue to [lab 3](../lab3/README.md)
