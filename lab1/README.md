# Lab 1: Creating a Serverless Function

## Go To Your Project

You will notice that your account has exactly one project in it named
**workshop**. This is the project that we will work from for the duration of
these exercises, and also contains the instructions for each of the labs.

**NOTE**: It will help to keep one tab open with the instructions, and another
tab open to work from.

Click on the **workshop** project in your "work" tab to begin.

![project](images/project.png)

Then open the **lab1** folder

![select_lab1](images/select_lab1.png)

## Create your function

We will create and edit our files using GitLab's built-in Web IDE. To get to it click on the **Web IDE** button in the upper right corner of your "work" tab.

![go_to_webide](images/go_to_webide.png)

Once in the GitLab web IDE, create a new file by clicking on the drop down arrow on the **lab1** folder:

![create_new_file_1](images/create_new_file_1.png)

and then selecting `new file` from the drop down:

![create_new_file_2](images/create_new_file_2.png)

In the modal dialog that comes up add the filename `hello/hello.py` to the directory that's already shown and click on the green **Create file** button:

![new_file_name](images/new_file_name.png)

This should set up the new file and set you up to edit it:

![enter_hello_code](images/enter_hello_code.png)

**NOTE**: You may have noticed that the filename `hello/hello.py` contains
a prepended directory name that does not already exist in the project. GitLab
will intepret this implicit directory creation for you so you don't have to
explicitly create a directory before creating a file in it. Think of it like
`mkdir -p` in \*nix.

Fill the new file in with the following contents:

```python
import json
import datetime


def endpoint(event, context):
    current_time = datetime.datetime.now().time()
    body = {
        "message": "Hello, the current time is " + str(current_time)
    }

    return body
```

**NOTE:** Python uses whitespace to denote blocks rather than brackets or some other form of delineation. Please be careful to preserve all whitespace in these blocks and copy them exactly as they appear.

To save and commit your changes click on the blue **Commit...** button:

![commit_hello_change_1](images/commit_hello_change_1.png)

GitLab shows you the change diffs and allows you to pick which files you want to stage for commit. Just click the blue **Commit...** button again:

![commit_hello_change_2](images/commit_hello_change_2.png)

Set the commit message to something like `Add hello.py`. Leave the radio button set to **Commit to master branch** (in the interest of time, since this is a lab repository, and since you will be the only editor on this repository, we will be working directly to the **master** branch for
this workshop. This is NOT a best practice.).

Finally, click the green **Stage & Commit** button.

![commit_hello_change_3](images/commit_hello_change_3.png)

You'll know your commit succeeded when you see the following in the bottom left corner of your tab:

![commit_hello_confirmation](images/commit_hello_confirmation.png)

## Create serverless.yml

So, now we have written a function! That's it right?

Well, not quite.

We will need to provide some metadata about the function such as the function's
name and runtime. To do this, create a new file in your repo's **root** directory:

![create_serverless_1](images/create_serverless_1.png)

Name it `serverless.yml` and click the green **Create file** button::

![create_serverless_2](images/create_serverless_2.png)

This should set up the new file and set you up to edit it:

![create_serverless_3](images/create_serverless_3.png)

Add the following contents:

```yaml
service: functions
description: "Deploying functions from GitLab using Knative"

provider:
  name: triggermesh
  registry-secret: gitlab-registry
  environment:
    FOO: BAR

functions:
  hello:
    source: hello
    runtime: https://gitlab.com/eggshell/knative-lambda-runtime/raw/master/python-3.7/buildtemplate.yaml
    description: "python Hello function with KLR template"
    buildargs:
     - DIRECTORY=hello
     - HANDLER=hello.endpoint
```

To save and commit your changes click on the blue **Commit...** button:

![commit_serverless_1](images/commit_serverless_1.png)

GitLab shows you the change diffs and allows you to pick which files you want to stage for commit. Just click the blue **Commit...** button again:

![commit_serverless_2](images/commit_serverless_2.png)

Set the commit message to something like `Add serverless.yml`. Leave the radio button set to **Commit to master branch** (in the interest of time, since this is a lab repository, and since you will be the only editor on this repository, we will be working directly to the **master** branch for
this workshop. This is NOT a best practice.).

Finally, click the green **Stage & Commit** button.

![commit_serverless_3](images/commit_serverless_3.png)

## Create a GitLab CI Configuration

Now that we have a function and some information about it, the last thing we'll
need is a method to deploy it, and that's where GitLab CI comes in.

Create a new file in your project's root directory:

![create_ciyaml_01](images/create_ciyaml_01.png)

We are creating a `.gitlab-ci.yml` so we can just click the quick button named **.gitlab-ci.yml** to have GitLab create it and name it appropriately:

![create_ciyaml_02](images/create_ciyaml_02.png)

This will give us a blank file, but note that there is also a **Choose a template...** drop-down menu available which offers many pre-defined pipeline templates for all sorts of use cases. For this lab we will just paste in our own pipeline definition (where the red arrow is pointing to):

![create_ciyaml_03](images/create_ciyaml_03.png)

Add the following contents:

```yaml
stages:
  - deploy-function

deploy-hello-function:
  stage: deploy-function
  environment: test
  image: gcr.io/triggermesh/tm:latest
  before_script:
    - echo $TMCONFIG > tmconfig
  script:
    - tm --config ./tmconfig deploy --wait; echo
```

Once you've added the contents, click on the blue **Commit...** button:

![commit_ciyaml_01](images/commit_ciyaml_01.png)

GitLab shows you the change diffs and allows you to pick which files you want to stage for commit. Just click the blue **Commit...** button again:

![commit_ciyaml_02](images/commit_ciyaml_02.png)

Set the commit message to something like `Add .gitlab-ci.yml`. Leave the radio button set to **Commit to master branch** (in the interest of time, since this is a lab repository, and since you will be the only editor on this repository, we will be working directly to the **master** branch for
this workshop. This is NOT a best practice.).

Finally, click the green **Stage & Commit** button.

![commit_ciyaml_03](images/commit_ciyaml_03.png)

## Check the pipeline

Click the **CI/CD** tab in the left-hand sidebar. This will bring you to the
**Pipelines** page, where you can view the status of your project's CI/CD jobs.
Click the top pipeline, which should say be in a **Pending** or **Running**
state. This will bring you to the job's output log.

When the job completes, you will get some output that looks like:

```shell
...
$ tm --config ./tmconfig deploy --wait
...
Service python-hello-hello URL: http://python-hello-hello-workshop-user1.k.triggermesh.io
Job succeeded
```

Pay attention to the second to last line, which contains your function's URL.
Copy it to your clipboard.

## Test the Function

To test out your function, open up a new broswer tab and make a request to the URL
obtained from the job output log. The page will load for a few seconds while a
container spins up to run your function. Once it does, the output will look like:

```shell
{"message": "Hello, the current time is 22:39:43.706024"}
```

If your page does not load or you receive a different response, please raise
your hand and one of the workshop TA's will come by.

**NOTE**: If you see the following error in the job output log, just ignore it:

```shell
ERROR: logging before flag.Parse: E0515 14:44:02.10659824 streamwatcher.go:109] Unable to decode an event from the watch stream: stream error: stream ID 17; INTERNAL_ERROR
```

This is not an error on your end, just a spurious bug.

## Summary

So, what exactly did we do here?

1. Wrote a serverless function to return the time in UTC in a JSON blob.
1. Defined a file which tells Knative which runtime your function uses, and
   what to call it when it is deployed.
1. Created a GitLab CI/CD configuration to deploy your function to Knative on
   TriggerMesh cloud.
1. Upon calling your deployed function, TriggerMesh cloud created a Kubernetes
   pod and a Kubernetes service to run your function in. This means that a
   container was created, filesystem laid down, and initialized with your
   python function as its initial process.
1. The function determines the current time in UTC and returns it to the
   caller (your browser).
1. Once the service has not received any more traffic over a certain interval,
   the container is destroyed, which means your time service has scaled down
   from 1 to 0.

So, why would we care about this? Serverless functions are meant to run on an
as-needed basis. If part of your application relies on computations that do not
need to be running 24/7, serverless functions provide an easy way to only spin
them up when you need them. This allows your infrastructure to not use extra
resources it doesn't need, and can prevent you from wasting CPU cycles.

We will demonstrate this in the next lab.

## Proceed to Lab 2

Continue to [lab 2](../lab2/README.md)
